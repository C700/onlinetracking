<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Gate;
use Illuminate\Support\Facades\Session;

class EnabledMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (Gate::allows('enabled',Auth::user())){
		
		    return $next($request);
	    }
	    Auth::logout();
    	Session::flush();
	    return redirect('/login')->with('warning','帳號可能因未啟用、無權限、或Session過期 請重新登入');
    }
}

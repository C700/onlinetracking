@extends('layouts.app')



@section('javascript')
<script>
	$(document).ready(function (e) {
	
	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});	            	 					            	 			            	 
});


function Check(){

	
	var cc_doctor = cc_name = cc_type = cc_sales = false;
	
	if($('#cc_name').val() == '' ){
		$('#v_name').text('◎');
		cc_name = true;
	}
	
	if($('#cc_type').val() == '' ){
		$('#v_type').text('◎');
		cc_type = true;
	}
	
	if($('#cc_sales').val() == '' ){
		$('#v_sales').text('◎');
		cc_sales = true;
	}
	
	if($('#cc_doctor').val() == '' ){
		$('#v_doctor').text('◎');
		cc_doctor = true;
	}
	
	if(cc_name || cc_type || cc_sales || cc_doctor){
		return false;
	}
	
	
	$('#check').submit();
	return false;
}

	</script>
@endsection

@section('content')

<div class="container">
	 <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            
			<div class="panel-heading">
				資訊填寫表
			</div>
			
			<div class="panel-body">
			
			<form id="check" action="{{url('/fill-form-with-qrcode/action_save')}}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div>&nbsp</div>
              
				<!--Subject ID-->
                <div class="form-group">
                    <label for="keyword" class="col-sm-3 control-label">Subject ID</label>

                    <div class="col-sm-6">
                        <input type="text" name="keyword" id="keyword" class="form-control" value="{{$subID}}" required readonly>
                    </div>
                </div>

			  
				<!--姓名-->
					<div class="form-group">
                        <b id="v_name" style="color:red"></b>
						<label  class="col-sm-3 control-label" for="keyword_name" >檢體名稱</label>
						
                        <div class="col-sm-6">
                            <input type="text" name="keyword_name" id="keyword_name" class="form-control " value="" >
                        </div>
						
                    </div>
				
				<!--檢體類別-->
					<div class="form-group">
                        <b id="v_type" style="color:red"></b>
						<label for="closing" class="col-sm-3 control-label" for="keyword_type">檢體類別</label>
						
                        <div class="col-sm-6">
                            <select class="form-control" name="keyword_type" id="keyword_type">
									<option value="" selected>請選取一個類別</option>
									@if(!empty($types))
										@foreach($types as $row)
											<option value="{{$row}}">{{$row}}</option>
										@endforeach
									@endif
									<option value="其他" >其他</option>
                            </select>
                        </div>
						
                    </div>
					
					
					<!--收案醫院-->
					<div class="form-group">
                        <label for="ERPnum" class="col-sm-3 control-label">收案醫院</label>
                        <div class="col-sm-6">
                            <input type="text" name="keyword_hos_name" id="keyword_hos_name" class="form-control" value="{{$hospitals['hos_name']}}" required readonly>
                        </div>
                    </div>
					
					<!--醫院地址
					<div class="form-group">
                        <label for="ERPnum" class="col-sm-3 control-label">醫院地址</label>
                        <div class="col-sm-6">
                            <input type="text" name="keyword_hos_addr" class="form-control" value="{{$hospitals['hos_add']}}"  >
                        </div>
                    </div>
					-->
					
					<!--收案醫師-->
					<div class="form-group">
                        <b id="v_doctor" style="color:red"></b>
						<label for="closing" class="col-sm-3 control-label">收案醫師</label>
						
                        <div class="col-sm-6">
                            <select class="form-control" name="keyword_doctor" id="keyword_doctor">
								<option value="" selected>請選取一位醫生</option>
								@if(!empty($hospitals['doc_name']))
									@foreach($hospitals['doc_name'] as $row)
										<option value="{{$row}}">{{$row}}</option>
									@endforeach
								@endif	
								<option value="其他" >其他</option>
                            </select>
                        </div>
						
                    </div>
					
					
					<!--負責業務-->
					<div class="form-group"> 
						<b id="v_sales" style="color:red"></b>					
						<label for="closing" class="col-sm-3 control-label" for="keyword_sales" >負責業務</label>
						
                        <div class="col-sm-6">
                            <select class="form-control"  name="keyword_sales" id="keyword_sales">
									<option value="">請選取一位業務</option>
									@if(!empty($sales))
										@foreach($sales as $row)
										<option value="{{$row}}" {{ strtolower($row) == $user ? 'selected' : '' }} >{{$row}}</option>
										@endforeach
									@endif
                            </select>
                        </div>
						
                    </div>
					
					<!--是否為急件-->
					<div class="form-group">
                        <label for="closing" class="col-sm-3 control-label">是否為急件</label>
                        <div class="col-sm-6">
                            <input class="w3-radio" type="radio" name="urgent" value="N" checked>否&nbsp
							<input class="w3-radio" type="radio" name="urgent" value="R" >是</p>
                        </div>
                    </div>
					
					<!--Note-->
					<div class="form-group">
                        <label for="closing" class="col-sm-3 control-label ">Note</label>
                        <div class="col-sm-6">
                       
							<textarea class="form-control" rows="3" name="keyword_note"></textarea>
                        </div>
                    </div>
					
                <!-- Search Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default" onclick="return Check()">
                            <i class="fa fa-btn fa-search"></i>Search
							
                        </button>
                    </div>
                </div>
            </form>
			
			
			
		</div></div></div></div>
@endsection


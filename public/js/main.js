$( document ).ready(function() {
   $('#showSearch').click(function(){ 
    if($('#showSearchPanel').css('display') == 'none'){
        $('#showSearchPanel').show();
        $('#showSearch i').attr('class', 'fa fa-btn fa-minus');
    }else{
        $('#showSearchPanel').hide();
        $('#showSearch i').attr('class', 'fa fa-btn fa-plus');
    }
   })
});
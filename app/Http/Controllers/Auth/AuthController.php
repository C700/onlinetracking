<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $username = 'username';

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/customers';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    protected function login(Request $request){
    	//ad domain & dn setup
	    $strDomain = "phalanx.com";
		$dn = "dc=phalanx,dc=com";
		
	    //get user info
		$user = $request->username."@$strDomain";
		$password = $request->password;
		
		//connect adldap server
		$ldap_connect = ldap_connect($strDomain) or die("無法連上$strDomain");
		if($ldap_connect){
			$ldap_bind = @ldap_bind($ldap_connect,$user,$password); // check account & password can pass server
			
			if($ldap_bind){
				//search username
				$ldap_search = @ldap_search($ldap_connect,$dn,"(samaccountname={$request->username})");
				if($ldap_search){
					//resource to array
					$data = ldap_get_entries($ldap_connect, $ldap_search);
					
					if($data['count'] == 0){
						return redirect()->back()->with('warning','找不到該AD帳號');
					}
					else {
						$user = User::where("email",$data[0]['mail'][0])->first();
						if(is_null($user)){
							$user = User::create([
								'username' => explode('@',$data[0]['mail'][0])[0],
								'email' => $data[0]['mail'][0],
								'password' => Hash::make('Ab123456'),
								'salesID' => "00000",
								'salesName' => explode('@',$data[0]['mail'][0])[0],
								'power' => '000'
							]);
							
						}
						Auth::login($user);
						Session::put("displayname",mb_convert_encoding($data[0]['displayname'][0],'UTF-8','BIG5'));
						return redirect()->route('customers.index');
					}
				}else return redirect()->back()->with('danger','無法查詢AD資料!');
				
			}else return redirect()->back()->with('warning',"<span style='color:red'>帳密錯誤</span>");
		}else return redirect()->back()->with('danger','無法連上 LDAP SERVER');
		ldap_close($ldap_connect);
    }
}

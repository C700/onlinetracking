
<div class="panel panel-default">
	<div class="panel-heading">
		<button type="button" id="showSearch">
			&nbsp 搜尋更多專案
		</button>
	</div>
	
	<div class="panel-body" style="display:none;" id="showSearchPanel">
		<!-- Display Validation Errors -->
		@include('common.errors')
		<ul class="nav nav-pills">
			<li class="active"><a data-toggle="pill" href="#tab_keyword"> 搜尋 (模糊比對)</a></li>
			<li><a data-toggle="pill" href="#tab_adsearch">進階搜尋  (精準比對)</a></li>
		</ul>
		
		<div class="tab-content"><!--tab begin-->
			<div id="tab_keyword" class="tab-pane fade in active">
				<!-- Search Customers Form -->
				<form action="{{ url('customer') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div>&nbsp</div>
					<!-- Key Words -->
					<div class="form-group">
						<label for="keyword" class="col-sm-3 control-label">請輸入關鍵字</label>
						
						<div class="col-sm-6">
							<input type="text" name="keyword" id="keyword" v-model="keyword" class="form-control" value="{{ old('keyword') }}">
						</div>
					</div>
					<!-- Search Button -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-6">
							<button type="submit" :disabled="!btn_status"  class="btn btn-default">
								<i class="fa fa-btn fa-search"></i>Search
							</button>
						</div>
					</div>
				</form>
			</div>
			<div id="tab_adsearch" class="tab-pane fade">
				<!--show AdSearch click -->
				<form action="{{ url('adsearch') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div id="AdSearch">
						<div>&nbsp </div>
						<!--
							<div class="form-group">
								<label for="ERPnum" class="col-sm-3 control-label">訂單編號</label>
								<div class="col-sm-6">
									<input type="text" name="orderNo" id="orderNo" class="form-control" value="" placeholder="ex: 222-2018010213">
								</div>
							</div>
						-->
						<div class="form-group">
							<label for="orderdate" class="col-sm-3 control-label">訂單日期</label>
							<div class="col-sm-6">
								起始：<input type="date" v-model="start"  name="orderdate1" id="orderdate1"  value=""><br>
								結束：<input type="date" v-model="end"  name="orderdate2" id="orderdate2"  value="">
							</div>
						</div>
						<div class="form-group">
							<label for="closing" class="col-sm-3 control-label">銷貨狀況</label>
							<div class="col-sm-6">
								<select class="form-control" v-model="close" id="closing" name="closing">
									<option value="0">全部</option>
									<option value="N">未結案</option>
									<option value="Y">已結案</option>
									<option value="y">強制結案</option>
									<option value="V">作廢</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="prod_id" class="col-sm-3 control-label">產品分類</label>
							<div class="col-sm-6">
								<select class="form-control" v-model="prod_id" id="prod_id" name="prod_id">
									<option value="0">全部</option>
									<option value="LHSP0004A">Cyto 檢測</option>
									<option value="LHSP0008A">Cyto 小兒檢測</option>
									<option value="LSNP0003">SNP癌症及慢性病風險基因檢測</option>
									<option value="LSNP0005">SNP肥胖風險基因檢測</option>
								</select>
							</div>
						</div>
					</div>
					<!-- Search Button -->
					<div class="form-group" id="adSearchBtn">
						<div class="col-sm-offset-3 col-sm-6">
							<button type="submit" :disabled="!btn_status" class="btn btn-default">
								<i class="fa fa-btn fa-search"></i>Search
							</button>
						</div>
					</div>
				</form></div>
		</div><!--tab end-->
	</div>
</div>
<hr>
@if(isset($keyword))
	
	<div class="alert alert-info" role="alert">
		<h4>搜尋的關鍵字為"{!! $keyword!!}"，共{{Session::get('count')}}筆紀錄</h4>
	</div>
@endif
<script src="{{asset('js/search_panel.vue.js')}}"></script>
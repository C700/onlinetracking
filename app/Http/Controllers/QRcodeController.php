<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Routing\Redirector;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\QRcode;

class QRcodeController extends Controller
{
    	
	protected $is_logined; //是否登入
    protected $enabled;// 使用者停權
    protected $power;//瀏覽權限，101:系統管理者；104: 可看所有客戶；108:只可看所屬客
    
    public function __construct()
    {
        $this->middleware('auth');
    }
	//QRcode填寫的表單
    public function index($subjectID)
    {
        $subID = trim(htmlspecialchars($subjectID, ENT_QUOTES, 'UTF-8'));
		
		//判斷是否有此資料
		$subID_exist = QRcode::subjectID_exist($subID);
		
		if($subID_exist){
			return redirect()->action('QRcodeController@qrcodelist')->with('status', 'SubjectID exist');
		}
		
        if( !empty($subID) ){
			$hospital = QRcode::getHospitalInfo($subID);
			
			if( $hospital['error'] != 1 )
			{
				$types = QRcode::getccType();
				$sales = QRcode::getccsales();

				//qrcodeinfo
				return view('qrcodeinfo',[
					'subID' => $subID,
					'types' => $types,
					'sales' => $sales,
					'hospitals' => $hospital,
					'user' => Auth::user()->username
				]);
			}else{
				return redirect()->action('QRcodeController@qrcodelist')->with('warning', 'hospital error ');
			}
        }else{
			return redirect()->action('QRcodeController@qrcodelist')->with('warning', 'SubjectID is null');
		}
    }
	//提交儲存資料進資料庫
	public function save(Request $request)
	{
		
		
		$CC_SENDCODE = test_input($request->input('keyword'));
		
		//dd($CC_SENDCODE);
		
		//判斷是否有此資料
		$subID_exist = QRcode::subjectID_exist($CC_SENDCODE);
		
	
		if($subID_exist ){
			
			return redirect()->action('QRcodeController@qrcodelist')->with('status', 'SubjectID exist');
		}else{
			
			$CC_SUBJECTID = 'QRcode';
			$CC_NAME = test_input($request->input('keyword_name'));
			$CC_NAME = mb_convert_encoding($CC_NAME,'BIG5','UTF-8');
			$CC_NR = $request->input('urgent');
			$CC_TYPE = test_input($request->input('keyword_type'));
			$CC_TYPE = mb_convert_encoding($CC_TYPE,'BIG5','UTF-8');
			$CC_SALES = $request->input('keyword_sales');
			$CC_HOSPITAL = test_input($request->input('keyword_hos_name'));
			$CC_HOSPITAL = mb_convert_encoding($CC_HOSPITAL,'BIG5','UTF-8');
			$CC_HOSPITALADDR = test_input($request->input('keyword_hos_addr'));
			$CC_HOSPITALADDR = mb_convert_encoding($CC_HOSPITALADDR,'BIG5','UTF-8');
			$CC_DOCTOR = test_input($request->input('keyword_doctor'));
			$CC_DOCTOR = mb_convert_encoding($CC_DOCTOR,'BIG5','UTF-8');
			$CC_TREASON = test_input($request->input('keyword_note'));
			$CC_TREASON = mb_convert_encoding($CC_TREASON,'BIG5','UTF-8');
			$CC_UPUSER = Auth::user()->username;
			
			$result = QRcode::save_qr_result($CC_SENDCODE, $CC_SUBJECTID, $CC_NAME, $CC_NR, $CC_TYPE, $CC_SALES, $CC_HOSPITAL, $CC_HOSPITALADDR, $CC_DOCTOR, $CC_TREASON, $CC_UPUSER);		
			return redirect()->action('QRcodeController@qrcodelist')->with('status', 'Inserted Successfully');
		
		}
	}
    //顯示歷史QRcode提交資料
	
	public function qrcodelist(Request $request)
	{
		$lists = QRcode::qrcodelist(Auth::user()->username);
		if(!empty($lists)){
			foreach($lists as $row)
			{
				$list[] = array(
					'CC_SENDCODE' => mb_convert_encoding($row['CC_SENDCODE'],'UTF-8','BIG5'),
					'CC_SUBJECTID' => mb_convert_encoding($row['CC_SUBJECTID'],'UTF-8','BIG5'),
					'CC_NAME' => mb_convert_encoding($row['CC_NAME'],'UTF-8','BIG5'),
					'CC_TYPE' => mb_convert_encoding($row['CC_TYPE'],'UTF-8','BIG5'),
					'CC_DOCTOR' => mb_convert_encoding($row['CC_DOCTOR'],'UTF-8','BIG5'),
					'CC_HOSPITAL' => mb_convert_encoding($row['CC_HOSPITAL'],'UTF-8','BIG5'),
					'CC_HOSPITALADDR' => mb_convert_encoding($row['CC_HOSPITALADDR'],'UTF-8','BIG5'),
					'CC_TREASON' => mb_convert_encoding($row['CC_TREASON'],'UTF-8','BIG5'),
					'CC_NR' => mb_convert_encoding($row['CC_NR'],'UTF-8','BIG5'),
					'CC_SALES' => mb_convert_encoding($row['CC_SALES'],'UTF-8','BIG5'),
					'CC_UPDATE' => mb_convert_encoding($row['CC_UPDATE'],'UTF-8','BIG5'),
					'CC_UPUSER' => mb_convert_encoding($row['CC_UPUSER'],'UTF-8','BIG5')
				);
			}
		}else{
			$list = [];
		}
		
		return view('qrcoderesult',[
			'datalist' => $list
		]);
	}
}

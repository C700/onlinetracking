<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>OnlineTracking</title>

	
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
	
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{ asset('/css/switch.css') }}" rel="stylesheet">
	
	<script src="{{asset('/js/vue.js')}}"></script>
	
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
		
            <div class="navbar-header">
				
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<div><h3> &nbsp &nbsp  <a href="{{ url('/customers') }}"> OnlineTracking </a></h3></div>
               
            </div>
			
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <!--<li><a href="{{ url('/register') }}">Register</a></li>-->
                    @else
                    
						<li>
							<a href="{{Auth::user()->Power->level == 1? url('/admin') : '#'}}"><span class="glyphicon glyphicon-user" ></span> {{Auth::user()->salesName}}({{Auth::user()->username}})</a>
						</li>
						<li><a href="{{ route('customers.index') }}"><span class="glyphicon glyphicon-list-alt"></span> 客戶 清單</a></li>
						<li><a href="{{ route('qrcodelist') }}"><span class="glyphicon glyphicon-qrcode"></span> QRcode 清單</a></li>
		                <li><a href="{{ route('analysis.index') }}"><span class="glyphicon glyphicon-object-align-bottom"></span> 分析圖表</a></li>
						{{--<li><a href="{{ route('changepwd') }}"><span class="glyphicon glyphicon-cog"></span> 密碼變更</a></li>--}}
						
						<li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-log-out"></i> 登出</a></li>
						<hr>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @include('layouts.alerts')
    @yield('content')
	
	<!-- JavaScripts -->	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="{{asset('/js/main.js')}}"></script>
    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    
     {{--<script src="{{ elixir('js/app.js') }}"></script> --}}
	<script type="text/javascript">     
	 $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      }); 
	  
	  
	</script>
	@yield('javascript')

</body>
</html>

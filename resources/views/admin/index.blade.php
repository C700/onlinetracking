@extends('layouts.app')

@section('content')
	
	<div class="container" id="container_admin">
		<div class="col-sm-offset-2 col-sm-8">
			<button class="btn btn-primary" style="margin-bottom: 10px" data-toggle="modal" data-target="#modal_admin_create">
				<span class="glyphicon glyphicon-plus">創建新帳號</span>
			</button>
			<a type="button" class="btn btn-success" style="margin-bottom: 10px;float: right;" href="{{route('power.index')}}">
				<span class="glyphicon glyphicon-eye-open">權限列表</span>
			</a>
			<div class="panel panel-default" style="overflow-x:scroll;">
				<table class="table">
					<thead>
						<th>使用者(業務代碼)/業務名稱</th>
						<th>Email/瀏覽權限</th>
						{{--<th>啟用</th>--}}
						{{--<th>操作</th>--}}
					</thead>
					<tbody id="table_contain">
						@foreach($users as $user)
						<tr style="color:{{$user->enabled?'green':'red'}}" @click="edit_admin('{{$user->id}}','{{$user->username}}','{{$user->email}}','{{$user->salesID}}','{{$user->salesName}}','{{$user->enabled}}','{{$user->power}}')" >
							<td>{{$user->salesName}}({{$user->salesID}})<br>{{$user->username}}</td>
							<td>{{$user->email}}<br>{!! @$powers[$user->power]?$user->power.'('.$powers[$user->power].')':"<span style='color:red'>未設定</span>"!!}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@include("admin.modal_admin_edit")
	</div>

@endsection
@include("admin.modal_admin_create")
@section("javascript")
	<script src="{{asset('js/admin.vue.js')}}"></script>
@endsection
@extends('layouts.app')

@section('content')
	
	<div class="container" id="container_power">
		<div class="col-sm-offset-2 col-sm-8">
			<button class="btn btn-primary" style="margin-bottom: 10px" data-toggle="modal" data-target="#modal_power_create">
				<span class="glyphicon glyphicon-plus">建立新權限</span>
			</button>
			<a type="button" class="btn btn-success" style="margin-bottom: 10px;float:right" href="{{route('admin.index')}}">
				<span class="glyphicon glyphicon-user">使用者列表</span>
			</a>
			<div class="panel panel-default" style="overflow-x:scroll;">
				<table class="table">
					<thead>
						<th>名稱</th>
						<th>權限</th>
					</thead>
					<tbody id="table_contain">
						@foreach($powers as $power)
						<tr @click="edit_power('{{$power->id}}','{{$power->name}}','{{$power->code}}','{{$power->level}}')">
							<td>{{$power->name}}({{$power->code}})</td>
							<td>{{$power->level?"全部資料":"部分資料"}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@include("admin.modal_power_edit")
	</div>

@endsection
@include("admin.modal_power_create")
@section("javascript")
	<script src="{{asset('js/power.vue.js')}}"></script>
@endsection
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-sm-offset-2 col-sm-8">
    @include("layouts.search_panel")
    
@if (count($customers) > 0 && !empty($customers))
    <div class="panel panel-default" style="overflow-x:scroll;">
            <table class="table"  >
                <thead>
                    <th style="width: 10%;">訂單狀況</th>
                    <th style="width: 25%;" class="table-text">客戶名稱</th>
                    <th style="width: 65%;" class="table-text">產品</th>
                    {{--<th style="width: 10%;" class="table-text">操作</th>--}}
                </thead>
                <tbody id="table_contain">
                @foreach ($customers as $row)
                    <tr onclick="show('{{$row->PO}}')">
                        <td>
							<a href="show/{{$row->PO}}">
							<!--是否已經結案(Y:已結案/N:未結案/y:強制結案/V:作廢)-->
							@if($row->PO_CLOSE == 'Y' )
								<span class="label label-success">已結案</span>

							@elseif($row->PO_CLOSE == 'y' ) 
								<span class="label label-primary">強制結案</span>
							
							@elseif($row->PO_CLOSE == 'V' ) 
								<span class="label label-default">作廢</span>
							@elseif($row->PO_CLOSE == 'N' ) 
								<span class="label label-warning">未結案</span>
							@endif
							</a>
						</td>
                        <td data-val="{{$row->PO}}" class="clickme">{{$row->CUS_NAME}}</td>
	                    <td data-val="{{$row->PO}}" class="clickme">{{$row->PROD_NAME}}</td>
{{--                        <td><a href="show/{{$row->PO}}"><i class="fa fa-external-link-square" aria-hidden="true"></i></a></td>--}}
                    </tr>
                @endforeach
                
                </tbody>
            </table>
            
    </div>
		    <div id="searching_text" style="font-size: 10vw; display: none">搜尋中...請稍後...</div>
@else    

	<div class="alert alert-warning" role="alert">
		<h4>No Search Result</h4>
	</div>
@endif
    </div>
</div>

@endsection

@section("javascript")
	<script type="text/javascript">
		function show(num) {
			location.href = "/show/"+num;
		}
		var searching_text = $("#searching_text");
		var start = 20;
		var toggle = @if(Session::get('count') > 20)true; @else false; @endif;
		$(window).on("scroll", function() {
		var scrollHeight = $(document).height();//頁面總長度
		var scrollPosition = $(window).height() + $(window).scrollTop();//手機螢幕高 + 目前畫面的top 位置
		if ((scrollHeight - scrollPosition) === 0) {
			if(toggle)searching_text.show(); else searching_text.hide();
			$.ajax({
				type:'post',
				url:'{{route("ajax.customers")}}',
				data:{
					start:start,
					mode:@php
						if(Session::has("keyword"))
							echo "'keyword'";
						else if(Session::has("adsearch"))
							echo "'adsearch'";
						else echo "'none'";
					@endphp
				},
				success:function (obj) {
					if(obj === "fail"){
						if(toggle)
							alert("用戶已停權");
						toggle=false;
						searching_text.hide();
						return ;
					}else if(obj === "empty" ){
						if(toggle)
							alert("無資料!");
						toggle = false;
						searching_text.hide();
						return ;
					}
					
					$.each(obj,function (index,val) {
						var close = "";
						switch(val.close){
							case 'Y':
								close = "<span class=\"label label-success\">已結案</span>";
								break;
							case 'y':
								close = "<span class=\"label label-primary\">強制結案</span>";
								break;
							case 'V':
								close = "<span class=\"label label-default\">作廢</span>";
								break;
							case 'N':
								close = "<span class=\"label label-warning\">未結案</span>";
								break;
						}
						searching_text.hide();
						$("#table_contain").append(
							`<tr onclick="show('${val.serial}')">
								<td><a href="show/${val.serial}">${close}</a></td>
								<td  data-val="${val.serial}" class="clickme">${val.c_name}</td>
	                            <td  data-val="${val.serial}" class="clickme">${val.p_name}</td>
							</tr>`
						);
					});
					start += 20;
				}
			})
		}
		});
	</script>
@endsection
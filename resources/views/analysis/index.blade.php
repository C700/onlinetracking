@extends('layouts.app')

@section('content')
	
	<div class="container">
		<h3>{{$date[0]}}年 {{$date[1]}}月統計圖</h3>{!! sizeof($locals) == 0?"<span style='color:red'>本月無資料</span>":""!!}<br> <input type="month" id="month" value="{{$date[0]."-".$date[1]}}" onchange="changeMonth(this)">
		
		<div class=" col-sm-12">
			@if(sizeof($locals) == 0)
				<canvas id="canvas_local" width="350" height="150"></canvas>
			@else
				<canvas id="canvas_local" width="350" height="{{sizeof($locals) * 140}}"></canvas>
				@endif
		</div>
		<div class=" col-sm-12">
			@if(sizeof($locals) == 0)
				<canvas id="canvas_close" width="350" height="150"></canvas>
			@else
				<canvas id="canvas_close" width="350" height="{{sizeof($closes) * 140}}"></canvas>
			@endif
		</div>
	</div>

@endsection
@section("javascript")
	<script type="text/javascript">
		var locals_label = {!! json_encode($locals)!!};
		var locals_data = {!! json_encode($locals_count)!!};
		var ctx1 = document.getElementById("canvas_local").getContext('2d');
		var canvas_local = new Chart(ctx1, {
			type: 'horizontalBar',
			data: {
				labels: locals_label,
				datasets: [{
					label: '訂單數量',
					backgroundColor: "red",
					borderColor: "red",
					borderWidth: 1,
					data: locals_data,
				}]
			},
			options: {
				elements: {
					rectangle: {
						borderWidth: 2,
					}
				},
				responsive: true,
				maintainAspectRatio: false,
				
				title: {
					display: true,
					text: '訂單地區統計長條圖'
				}
			}
		});

		var closes_label = {!! json_encode($closes)!!};
		var closes_data = {!! json_encode($closes_count)!!};
		var ctx1 = document.getElementById("canvas_close").getContext('2d');
		var canvas_close = new Chart(ctx1, {
			type: 'horizontalBar',
			data: {
				labels: closes_label,
				datasets: [{
					label: '訂單數量',
					backgroundColor: "red",
					borderColor: "red",
					borderWidth: 1,
					data: closes_data
				}]
			},
			options: {
				elements: {
					rectangle: {
						borderWidth: 2,
					}
				},
				responsive: true,
				maintainAspectRatio: false,
				
				title: {
					display: true,
					text: '訂單狀態統計長條圖'
				}
			}
		});
		
		function changeMonth(obj){
			location.replace('/analysis/'+$(obj).val());
		}
		
	</script>
@endsection
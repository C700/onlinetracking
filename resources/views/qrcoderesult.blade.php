@extends('layouts.app')

@section('content')

<div class="container">
	@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
	@endif
    <div class="col-sm-offset-2 col-sm-8">
@if (count($datalist) > 0 && !empty($datalist))
	@foreach ($datalist as $row)
	@if ($row['CC_SUBJECTID'] == 'QRcode')
    <div class="panel panel-default">
    	<div class="panel-heading">收檢編號：{{$row['CC_SENDCODE']}}</div>
        <div class="panel-body">
			<p>訂單狀態：<span class="label label-success"> 資料已送出</span></p>
            <p>客戶姓名：{{$row['CC_NAME']}}</p>
            <p>檢體類別：{{$row['CC_TYPE']}}</p>
            <p>收案醫院：{{$row['CC_HOSPITAL']}}</p>
            <!--<p>醫院地址：{{$row['CC_HOSPITALADDR']}}</p>-->
            <p>收案醫師：{{$row['CC_DOCTOR']}}</p>
            <p>負責業務：{{$row['CC_SALES']}}</p>
            <p>是否急件：
				@if($row['CC_NR']=='R')
					<span class="label label-danger"> 急件</span>
				@else
					否
				@endif
			</p>
            <p>備註：{{$row['CC_TREASON']}}</p>
			<hr>
			<p>資料上傳人員：{{$row['CC_UPUSER']}}</p>
			<p>資料填寫時間：{{$row['CC_UPDATE']}}</p>
        </div>
    </div>
    @endif
    @endforeach
@else
   <div class="alert alert-warning" role="alert">
		<h4>No Search Result</h4>
	</div>
@endif
    </div>
</div>
@endsection
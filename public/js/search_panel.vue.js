
var tab_keyword = new Vue({
	el: "#tab_keyword",
	data:{
		keyword:""
	},
	computed:{
		btn_status:function () {
			return  this.keyword !== "";
		}
	}
});
var tab_adsearch = new Vue({
	el: "#tab_adsearch",
	data:{
		start:"",
		end:"",
		close:"",
		prod_id:"",
	},
	computed:{
		btn_status:function () {

			return  this.start !== "" || this.end !== "" || this.close !== "" || this.prod_id !== "";
		}
	}
});
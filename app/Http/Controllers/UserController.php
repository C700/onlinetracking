<?php

namespace App\Http\Controllers;
use App\Power;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tracy\Debugger;

class UserController extends Controller
{
    public function __construct()
    {
//    	Debugger::enable();
//	    Debugger::$strictMode = true;
    	
        $this->middleware('auth');
    }
	
	/**
	 * 使用者顯示頁面
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function index()
    {
        foreach(Power::select('code','name')->get() as $p){
            $powers[$p->code] = $p->name;
	    }
	    $users = User::all();//抓使用者資料
	    return view('admin.index', compact('users','powers'));
    }
	
	/**
	 * 建立新帳號
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function create(Request $request)
    {
	    $user_info = $request->all();
	    $user_info['password'] = Hash::make("Ab123456");//密碼加密，但因為改用AD所以用不到了
	    if(User::create($user_info))
		    return redirect('admin')->with('success','新增成功');
	    else
	        return redirect()->back()->with('danger','新增失敗，請通知系統管理員');
    }
	
	/**
	 * 使用者更新資料
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(Request $request)
    {
	    if (User ::find($request -> id) -> update($request -> all())) {
		    return redirect() -> back() -> with('success', '更新成功');
	    }
	    return redirect() -> back() -> with('danger', '更新失敗，請通知系統管理員');
    }
}

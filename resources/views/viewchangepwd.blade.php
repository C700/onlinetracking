@extends('layouts.app')



@section('javascript')
<script>



function Check()
{
	
	var new_Password = $('#new_Password').val();
	var new_Password_confirm = $('#new_Password_confirm').val();

	if(new_Password == '' || new_Password_confirm  == ''){
		alert('欄位不能為空');return false;
	}
	
	var pattern = /^[0-9A-Za-z]*$/;
	if(!pattern.test(new_Password)){
		alert('密碼須為 /^[0-9A-Za-z]*$/');return false;
	}
	
	if(new_Password.length < 4 ){
		alert('密碼長度 < 4 , 長度過短');return false;
	}
	
	if(new_Password.length > 16){
		alert('密碼長度 > 16 , 長度過長');return false;
	}

	
	
	if( new_Password == new_Password_confirm ){
		$('#changepwd').submit();
	}
	

	return false;
}

	</script>
@endsection

@section('content')

<div class="container">
    
             <div class="panel panel-default">
                <div class="panel-heading">密碼變更</div>
                <div class="panel-body">
                    <form id="changepwd" class="form-horizontal" role="form" method="POST" action="{{ url('/changepwdsave') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">新密碼編號</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="new_Password" id="new_Password" value="">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">新密碼確認</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="new_Password_confirm" id="new_Password_confirm" value="">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                              
								<button type="submit" class="btn btn-primary" onclick="return Check()">
									<i class="fa fa-btn fa-search"></i>變更密碼							
								</button>
                            </div>
                        </div>
						
						
                    </form>
                </div>
            </div>
			<div class="alert alert-info" role="alert">
							<a href="#" class="alert-link">*如遺忘密碼 , 請聯絡大數據部*</a>
						</div>
</div>
@endsection
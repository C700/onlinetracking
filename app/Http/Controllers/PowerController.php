<?php

namespace App\Http\Controllers;

use App\Power;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class PowerController extends Controller
{
	public function __construct()
	{
//    	Debugger::enable();
//	    Debugger::$strictMode = true;
	}
	
	/**
	 * 權限顯示頁面
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function index()
	{
		$powers = Power::all();//抓使用者資料
		return view('admin.power', compact('powers'));
	}
	
	/**
	 * 建立新權限
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function create(Request $request)
	{
		$info = $request->all();
		if(Power::where('code',$info['code'])->orWhere('name',$info['name'])->count() > 0){
			return redirect()->back()->with('warning','代碼或名稱重複，請重新設定代碼');
		}
		if(Power::create($info))
			return redirect('power')->with('success','新增成功');
		else
			return redirect()->back()->with('danger','新增失敗，請通知系統管理員');
		
	}
	/**
	 * 權限更新資料
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request)
	{
		if(Power::where('id','!=',$request->id)->Where(function ($query) use($request){
				$query->where('code',$request->code)->orWhere('name',$request->name);
			})->count() > 0)
			return redirect()->back()->with('warning','名稱與代碼不可以重複');
		// Start transaction!
		DB::beginTransaction();
		try{
			$power = Power::find($request -> id);
			
			User::where('power',$power->code)->update(['power'=>$request->code]);
			$power->code = $request->code;
			$power->name = $request->name;
			$power->level = $request->level;
			$power->save();
		}catch (\Exception $e){
			//reset data when transaction fail
			DB::rollback();
			return redirect() -> back() -> with('danger', '更新失敗，請通知系統管理員');
		}
		//compelete transaction
		DB::commit();
		return redirect() -> back() -> with('success', '更新成功');
		
	}
}

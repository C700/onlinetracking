
{{--<div class="alert alert-primary" role="alert">--}}
	{{--This is a primary alert—check it out!--}}
{{--</div>--}}
{{--<div class="alert alert-secondary" role="alert">--}}
	{{--This is a secondary alert—check it out!--}}
{{--</div>--}}
@if(session("success"))

<div class="alert alert-success" role="alert">
	{!! session("success") !!}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
@if(session("danger"))
<div class="alert alert-danger" role="alert">
	{!! session("danger") !!}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
@if(session("warning"))
<div class="alert alert-warning" role="alert">
	{!! session("warning") !!}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
{{--<div class="alert alert-info" role="alert">--}}
	{{--This is a info alert—check it out!--}}
{{--</div>--}}
{{--<div class="alert alert-light" role="alert">--}}
	{{--This is a light alert—check it out!--}}
{{--</div>--}}
{{--<div class="alert alert-dark" role="alert">--}}
	{{--This is a dark alert—check it out!--}}
{{--</div>--}}
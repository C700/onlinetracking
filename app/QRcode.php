<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class QRcode extends Model
{
    //獲取檢體類別選單
    public static function getccType()
    {        
        $types = DB::connection('sqlsrveip')->table('ColContain')->where('ColID','=',25)
            ->orderBy('sortNO', 'asc')->lists('C_ITEM');
		foreach($types as $value)
		{
			$type[] = mb_convert_encoding($value,'UTF-8','BIG5');
		}

        return $type;
    }
	
	//獲取業務選單
	public static function getccSales()
	{
		$sales = DB::connection('sqlsrveip')->table('ColContain')->where('ColID','=',26)
            ->orderBy('sortNO', 'asc')->lists('C_ITEM');
		foreach($sales as $value)
		{
			$sale[] = mb_convert_encoding($value,'UTF-8','BIG5');
		}
		
        return $sale;
	}
	
	//獲取醫院，醫院地址，醫師名
	public static function getHospitalInfo($id)
	{
		$caseInfo = [];
		
		$url = "http://192.168.11.156/designkit/public/SubjectIDHub/subjectidapigetGO/$id";
		$headers = get_headers($url);
		
		$response_code = substr($headers[0], 9, 3);
		if($response_code == '200'){
			$info = file_get_contents($url);
			
			if( $info != '-1001')
			{
				$arr_temp = explode('<br>', $info)[1];
				$arr_temp = explode('~', $arr_temp);

				foreach($arr_temp as $value)
				{
					if($value){
						$doctor[] = explode('|', $value)[4];
					}
				}
				
				$caseInfo['hos_name'] = explode('=', explode('|', explode('~', $info)[0])[5])[1];
				$caseInfo['hos_add'] = explode('|', explode('~', $info)[1])[5];
				$caseInfo['doc_name'] = $doctor;
				$caseInfo['error'] = 0;
			}
			else
			{
				$caseInfo['error'] = 1;
			}
		}else{
			$caseInfo['error'] = 1;
		}
		return $caseInfo;
		
	}
	//提交儲存資料進資料庫
	public static function save_qr_result($cc_sendcode, $cc_subjectid, $cc_name, $cc_nr, $cc_type, $cc_sales, $cc_hospital, $cc_hospitaladdr, $cc_doctor, $cc_treason, $cc_upuser)
	{
		$sql= "OPEN SYMMETRIC KEY SecretKEY DECRYPTION BY PASSWORD = '97007330';Insert into TBL_CUSTOMER_X (CC_SENDCODE,CC_SUBJECTID , CC_NAME, CC_NR, CC_TYPE, CC_SALES, CC_HOSPITAL , CC_HOSPITALADDR, CC_DOCTOR, CC_TREASON, CC_UPUSER) Values ('$cc_sendcode','$cc_subjectid',EncryptByKey(Key_GUID('SecretKEY'), '$cc_name'),'$cc_nr', EncryptByKey(Key_GUID('SecretKEY'), '$cc_type') ,'$cc_sales',EncryptByKey(Key_GUID('SecretKEY'), '$cc_hospital'),EncryptByKey(Key_GUID('SecretKEY'), '$cc_hospitaladdr'),EncryptByKey(Key_GUID('SecretKEY'), '$cc_doctor'),EncryptByKey(Key_GUID('SecretKEY'), '$cc_treason'),'$cc_upuser')";
		
		$db = DB::connection('sqlsrvqr')->getPdo();
		
		$db->query($sql);
		
		return $sql;
	}
	//顯示歷史QRcode提交資料
	public static function qrcodelist($sales)
	{
		//$sales = 'JackPao';
		if($sales == 'wilsonhsu'){
			$sql ="OPEN SYMMETRIC KEY SecretKEY DECRYPTION BY PASSWORD = '97007330'
				SELECT [CC_SENDCODE],[CC_SUBJECTID]
                    ,CONVERT(varchar(50), DecryptByKey(CC_NAME)) CC_NAME 
                    ,CONVERT(varchar(50), DecryptByKey([CC_TYPE])) [CC_TYPE] 
                    ,CONVERT(varchar(50), DecryptByKey([CC_DOCTOR])) [CC_DOCTOR] 
                    ,CONVERT(varchar(100), DecryptByKey([CC_HOSPITAL])) [CC_HOSPITAL] 
                    ,CONVERT(varchar(100), DecryptByKey([CC_HOSPITALADDR])) [CC_HOSPITALADDR] 
                    ,CONVERT(varchar(200), DecryptByKey([CC_TREASON])) [CC_TREASON]
                    ,[CC_NR] 
                    ,[CC_SALES] 
                    ,[CC_UPDATE] 
                    ,[CC_UPUSER] 
                FROM [CytoDB].[dbo].[TBL_CUSTOMER_X] where  CC_SUBJECTID ='QRcode' ORDER BY CC_UPDATE DESC ";
			
		}else{
			$sql ="OPEN SYMMETRIC KEY SecretKEY DECRYPTION BY PASSWORD = '97007330'
				SELECT [CC_SENDCODE],[CC_SUBJECTID]
                    ,CONVERT(varchar(50), DecryptByKey(CC_NAME)) CC_NAME 
                    ,CONVERT(varchar(50), DecryptByKey([CC_TYPE])) [CC_TYPE] 
                    ,CONVERT(varchar(50), DecryptByKey([CC_DOCTOR])) [CC_DOCTOR] 
                    ,CONVERT(varchar(100), DecryptByKey([CC_HOSPITAL])) [CC_HOSPITAL] 
                    ,CONVERT(varchar(100), DecryptByKey([CC_HOSPITALADDR])) [CC_HOSPITALADDR] 
                    ,CONVERT(varchar(200), DecryptByKey([CC_TREASON])) [CC_TREASON]
                    ,[CC_NR] 
                    ,[CC_SALES] 
                    ,[CC_UPDATE] 
                    ,[CC_UPUSER] 
                FROM [CytoDB].[dbo].[TBL_CUSTOMER_X] where CC_SALES='$sales' and CC_SUBJECTID ='QRcode' ORDER BY CC_UPDATE DESC ";
			
		}
		
		
		
		$db = DB::connection('sqlsrvqr')->getPdo();
		
		$query = $db->query($sql);
		$datalist = $query->fetchAll();
		
		return $datalist;
	}

	//check SubjectID是否已存在
	public static function subjectID_exist($subid)
	{
		$sql ="OPEN SYMMETRIC KEY SecretKEY DECRYPTION BY PASSWORD = '97007330'
				SELECT * FROM [CytoDB].[dbo].[TBL_CUSTOMER_X] where CC_SENDCODE ='$subid'";
		
		$db = DB::connection('sqlsrvqr')->getPdo();
		$query = $db->query($sql);
		
		//dd($query);
		if($result = $query->fetch())
		{
			return TRUE;
		}

		return FALSE;
	}

}

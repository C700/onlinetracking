<div class="modal fade" id="modal_power_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{Form::open(['method'=>'patch','route'=>'power.update'])}}
			{{Form::text("id","",['id'=>'id','hidden'=>'hidden','v-model'=>'id'])}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title" id="exampleModalLabel">權限資料更新</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom: 10px">
					{{Form::label("name","名稱 ： ",['class'=>'control-label'])}}
					{{Form::text("name","",['placeholder'=>'主管','required'=>'required','id'=>'name','style'=>'width:70%','v-model'=>'name'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("code","代碼 ： ",['class'=>'control-label'])}}
					{{Form::number("code","",['required'=>'required','id'=>'code','style'=>'width:70%','v-model'=>'code','max'=>'127','title'=>"請輸入3位數字"])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("level","權限設定 ： ",['class'=>'control-label'])}}
					{{Form::radio('level', '0' , null, ['class' => 'w3-check','id'=>'level0','v-model'=>'level'])}}部分資料
					{{Form::radio('level', '1', null , ['class' => 'w3-check','required'=>'required','id'=>'level1','v-model'=>'level'])}}全部資料
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				<button type="submit" class="btn btn-success">儲存</button>
			</div>
			{{Form::close()}}
		</div>
	</div>
</div>
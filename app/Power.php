<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Power extends Model
{
    protected $table = "power";
    
    protected $fillable =[
    	'id','name','level','created_at','code','updated_at'
    ];
}

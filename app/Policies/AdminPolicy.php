<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function admin(User $user){
    	return $user->Power->level === 1;
    }
    
    public function enabled(User $user){
    	return $user->enabled === 1;
    }
}

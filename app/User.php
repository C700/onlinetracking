<?php

namespace App;

use App\Task;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'salesID', 'salesName', 'enabled', 'power'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	/**
	 * 取得power相關資訊
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
    public function Power()
    {
        return $this->hasone('App\Power','code','power');
    }
}

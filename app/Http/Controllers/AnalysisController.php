<?php

namespace App\Http\Controllers;

use App\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 *
 * Class AnalysisController
 * @package App\Http\Controllers
 */
class AnalysisController extends Controller
{
	/**
	 * 分析頁，透過get取得其他月份資料
	 *
	 * @param null $getmonth
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function index($getmonth = null)
	{
		Session ::forget('keyword');
		Session ::forget('adsearch');
		if(is_null($getmonth)){
			$date = [
				Carbon::now()->year,
				Carbon::now()->month
			];
		}else{
			$date = (explode("-",$getmonth));
		}
		$searchDate =(Carbon::createFromDate($date[0],$date[1],1));
		$salesID = Auth::user()->Power->level?null:Auth::user()->salesID;//抓取登入者權限
		
		if ($salesID) {
			$customers = Customer ::where('SALESID', '=', $salesID)
				->whereBetween('PO_DATE',[$searchDate,$searchDate->addMonth()->firstOfMonth()])
				-> orderBy('PO_DATE', 'desc') -> get();
		} else {
			$customers = Customer ::orderBy('PO_DATE', 'desc')
				->whereBetween('PO_DATE',[$searchDate,$searchDate->firstOfMonth()])-> get();
		}
		foreach ($customers as $key => $customer) {
			//字型編碼轉化
			$customers[$key] -> TYPE = mb_convert_encoding($customer -> TYPE, 'UTF-8', 'BIG5');
			$customers[$key] -> PROD_NAME = mb_convert_encoding($customer -> PROD_NAME, 'UTF-8', 'BIG5');
			$customers[$key] -> CUS_NAME = mb_convert_encoding($customer -> CUS_NAME, 'UTF-8', 'BIG5');
			$customers[$key] -> LOCAL = mb_convert_encoding($customer -> LOCAL, 'UTF-8', 'BIG5');
			$customers[$key] -> SALESNAME = mb_convert_encoding($customer -> SALESNAME, 'UTF-8', 'BIG5');
			$customers[$key] -> NOTE = mb_convert_encoding($customer -> NOTE, 'UTF-8', 'BIG5');

		}
		$locals = [];
		$locals_count = [];
		foreach ($customers->groupBy("LOCAL") as $key =>$value){
			$locals[] = $key;
			$locals_count[] = $value->count();
		}
		
		$closes = [];
		$closes_count = [];
		foreach ($customers->groupBy("PO_CLOSE") as $key =>$value){
			$closes[] = $this->close_name($key);
			$closes_count[] = $value->count();
		}
		
		return view('analysis.index', compact('locals','locals_count','closes','closes_count','date'));
		
	}
	
	/**
	 * 將PO_CLOSE轉成文字表達
	 *
	 * @param $close
	 * @return string
	 */
	public function close_name($close){
		switch ($close){
			case 'Y':
				return "已結案";
				break;
			case 'y':
				return "強制結案";
				break;
			case 'N':
				return "未結案";
				break;
			case 'V':
				return "作廢";
				break;
			default:
				return "";
				break;
		}
	}
}

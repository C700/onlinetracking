
new Vue({
	el:"#container_power",
	data:{
		id:"",
		name:"",
		code:"",
		level:"",
	},
	methods:{
		edit_power(id,name,code,level) {
			this.id=id;
			this.name=name;
			this.code=code;
			this.level=level;
			$("#modal_power_edit").modal('show');
		}
	}
});
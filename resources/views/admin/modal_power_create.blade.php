<div class="modal fade" id="modal_power_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{Form::open(['method'=>'post','route'=>'power.create'])}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title" id="exampleModalLabel">創建新權限</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom: 10px">
					{{Form::label("name","名稱 ： ",['class'=>'control-label'])}}
					{{Form::text("name","",['placeholder'=>'主管級','required'=>'required','id'=>'name_create','style'=>'width:70%'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("code","代碼 ： ",['class'=>'control-label'])}}
					{{Form::number("code","",['placeholder'=>'101','required'=>'required','id'=>'code_create','style'=>'width:70%'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("level","權限設定 ： ",['class'=>'control-label'])}}
					{{Form::radio('level', '0' , null, ['class' => 'w3-check','id'=>'power0_create'])}}部分資料
					{{Form::radio('level', '1', null , ['class' => 'w3-check','required'=>'required','id'=>'power1_create'])}}全部資料
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				<button type="submit" class="btn btn-success">新增</button>
			</div>
			{{Form::close()}}
		</div>
	</div>
</div>
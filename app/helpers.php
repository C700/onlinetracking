<?php
/*
This prevents attackers from exploiting the code 
by injecting HTML or Javascript code 
(Cross-site Scripting attacks)
*/
function test_input($str) {
    $str = trim($str);
    $str = stripslashes($str);
    $str = htmlspecialchars($str);
    return $str;
}

/*
檢查是否符合訂單編號格式: ex:222-2017110801
*/
function is_orderNo( $str )
{
   return preg_match("/^\d{3}-\d{10}$/", $str);
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Session;

class Customer extends Model
{
    //MSSQL連線資料 .env有詳細資訊
    protected $connection = 'sqlsrv';
    protected $table = 'tbl_POHistory';
    protected $primaryKey = 'PO';
    protected $fillable = [
        'PO','PO_TYPE','PO_ID','PO_SEQ','CUS_NAME','PROD_ID','PROD_NAME', 'SALESNAME', 'PO_DATE'
    ];
	
	/**
	 * 獲取所有客戶的訂單，只可查看所屬客戶，且針對第offset比開始的資料做limit筆撈取
	 *
	 * @param $salesID
	 * @param int $offset
	 * @param int $limit
	 * @return \Illuminate\Support\Collection
	 */
    public static function getCustomers($salesID,$offset = 0,$limit=20)
    {
    	
        if($salesID){
            $customers = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
		        ->orderBy('PO_DATE', 'desc')->offset($offset)->limit($limit)->get();
	        $count = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)->count();
        }else{
            $customers = DB::connection('sqlsrv')->table('tbl_POHistory')
                ->orderBy('PO_DATE', 'desc')->offset($offset)->limit($limit)->get();
	        $count = DB::connection('sqlsrv')->table('tbl_POHistory')->count();
        }
	    Session::put("count",$count);//view要顯示比數用的
        return collect($customers);
    }
	
	/**
	 * 關鍵字搜尋，且針對第offset比開始的資料做limit筆撈取
	 *
	 * @param $salesID
	 * @param $keyword
	 * @param int $offset
	 * @param int $limit
	 * @return \Illuminate\Support\Collection
	 */
    public static function getCustomersByKeyWord($salesID, $keyword,$offset = 0,$limit=20)
    {
        $keyword = test_input($keyword);
//        $keyword = mb_convert_encoding($keyword,'BIG5','UTF-8');
        if(is_orderNo( $keyword ))
            $keyword = str_replace('-','|',$keyword);
        if(!empty( $keyword )){
            if(!empty($salesID)){
                $customers = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
	                ->Where(function ($query) use ($keyword) {
		                $query->where('SALESNAME','like', '%'.$keyword.'%')
			                ->orWhere('SALESID','like', '%'.$keyword.'%')
			                ->orWhere('PO','like', '%'.$keyword.'%')
			                ->orWhere('PO_TYPE','like', '%'.$keyword.'%')
			                ->orWhere('PO_ID','like', '%'.$keyword.'%')
			                ->orWhere('PO_SEQ','like', '%'.$keyword.'%')
			                ->orWhere('CUS_NAME','like', '%'.$keyword.'%')
			                ->orWhere('PO_DATE','like', '%'.$keyword.'%')
			                ->orWhere('PROD_NAME','like', '%'.$keyword.'%')
			                ->orWhere('LOCAL','like', '%'.$keyword.'%');
	                })
	                ->offset($offset)->limit($limit)
                    ->orderBy('PO_DATE', 'desc')->get();
	            $count = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
		            ->Where(function ($query) use ($keyword) {
			            $query->where('SALESNAME','like', '%'.$keyword.'%')
				            ->orWhere('SALESID','like', '%'.$keyword.'%')
				            ->orWhere('PO','like', '%'.$keyword.'%')
				            ->orWhere('PO_TYPE','like', '%'.$keyword.'%')
				            ->orWhere('PO_ID','like', '%'.$keyword.'%')
				            ->orWhere('PO_SEQ','like', '%'.$keyword.'%')
				            ->orWhere('CUS_NAME','like', '%'.$keyword.'%')
				            ->orWhere('PO_DATE','like', '%'.$keyword.'%')
				            ->orWhere('PROD_NAME','like', '%'.$keyword.'%')
				            ->orWhere('LOCAL','like', '%'.$keyword.'%');
		            })->count();
            }else{
                $customers = DB::connection('sqlsrv')->table('tbl_POHistory')
                    ->where('SALESNAME','like', '%'.$keyword.'%')
                    ->orwhere('SALESID','like', '%'.$keyword.'%')
                    ->orwhere('PO','like', '%'.$keyword.'%')
                    ->orwhere('PO_TYPE','like', '%'.$keyword.'%')
                    ->orwhere('PO_ID','like', '%'.$keyword.'%')
                    ->orwhere('PO_SEQ','like', '%'.$keyword.'%')
                    ->orwhere('CUS_NAME','like', '%'.$keyword.'%')
                    ->orwhere('PO_DATE','like', '%'.$keyword.'%')
                    ->orwhere('PROD_NAME','like', '%'.$keyword.'%')
                    ->orwhere('LOCAL','like', '%'.$keyword.'%')
	                ->offset($offset)->limit($limit)
                    ->orderBy('PO_DATE', 'desc')->get();
	            $count = DB::connection('sqlsrv')->table('tbl_POHistory')
		            ->where('SALESNAME','like', '%'.$keyword.'%')
		            ->orwhere('SALESID','like', '%'.$keyword.'%')
		            ->orwhere('PO','like', '%'.$keyword.'%')
		            ->orwhere('PO_TYPE','like', '%'.$keyword.'%')
		            ->orwhere('PO_ID','like', '%'.$keyword.'%')
		            ->orwhere('PO_SEQ','like', '%'.$keyword.'%')
		            ->orwhere('CUS_NAME','like', '%'.$keyword.'%')
		            ->orwhere('PO_DATE','like', '%'.$keyword.'%')
		            ->orwhere('PROD_NAME','like', '%'.$keyword.'%')
		            ->orwhere('LOCAL','like', '%'.$keyword.'%')->count();
            }
            
        }else{
            if(!empty($salesID)){
				$customers = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
					->orderBy('PO_DATE', 'desc')->offset($offset)->limit($limit)->get();
	            $count = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)->count();
			}
        }
        
	    Session::put("count",$count);//view要顯示數量
        return collect($customers);
    }
	
	/**
	 * 進階搜尋=>訂單編號搜尋，日期範圍搜尋，銷貨況狀，產品分類
	 *
	 * @param $salesID
	 * @param $orderNo
	 * @param $date1
	 * @param $date2
	 * @param $closing
	 * @param $prod_id
	 * @param int $offset
	 * @param int $limit
	 * @return \Illuminate\Support\Collection
	 */
    public static function getResultByAdSearch($salesID, $orderNo, $date1, $date2, $closing, $prod_id,$offset = 0,$limit=20)
    {
        if($closing === 0){
            $arr_closing[0] = '<>';$arr_closing[1] = 0;
        }else{
            $arr_closing[0] = '=';$arr_closing[1] = $closing;
        }
        //$prod_id = mb_convert_encoding($prod_id,'BIG5','UTF-8');
        if($prod_id === 0){
            $arr_prod_id[0] = '<>';$arr_prod_id[1] = 0;
        }else{
            $arr_prod_id[0] = '=';$arr_prod_id[1] = $prod_id;
        } 
		
        if(!empty($salesID)){
            $customers = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
                ->where('PO','like', '%'.$orderNo.'%')
                ->whereRaw('PO_DATE >=? and PO_DATE <=? ', [$date1, $date2])
                ->where('PO_CLOSE',$arr_closing[0], $arr_closing[1])
                ->where('PROD_ID',$arr_prod_id[0], $arr_prod_id[1])
	            ->offset($offset)->limit($limit)
                ->orderBy('PO_DATE', 'desc')->get();
	        $count = DB::connection('sqlsrv')->table('tbl_POHistory')->where('SALESID','=',$salesID)
		        ->where('PO','like', '%'.$orderNo.'%')
		        ->whereRaw('PO_DATE >=? and PO_DATE <=? ', [$date1, $date2])
		        ->where('PO_CLOSE',$arr_closing[0], $arr_closing[1])
		        ->where('PROD_ID',$arr_prod_id[0], $arr_prod_id[1])->count();
        }else{
            $customers = DB::connection('sqlsrv')->table('tbl_POHistory')
                ->where('PO','like', '%'.$orderNo.'%')
                ->whereRaw('PO_DATE >=? and PO_DATE <=? ', [$date1,$date2])
                ->where('PO_CLOSE',$arr_closing[0], $arr_closing[1])
                ->where('PROD_ID',$arr_prod_id[0], $arr_prod_id[1])
	            ->offset($offset)->limit($limit)
                ->orderBy('PO_DATE', 'desc')->get();
	        $count = DB::connection('sqlsrv')->table('tbl_POHistory')
		        ->where('PO','like', '%'.$orderNo.'%')
		        ->whereRaw('PO_DATE >=? and PO_DATE <=? ', [$date1,$date2])
		        ->where('PO_CLOSE',$arr_closing[0], $arr_closing[1])
		        ->where('PROD_ID',$arr_prod_id[0], $arr_prod_id[1])->count();
        }
	    Session::put("count",$count);
        
        return collect($customers);
    }
}

<?php
Route::post('adlogin',['as'=>'adlogin','uses'=>'Auth\AuthController@login']);


    Route::get('/', function () {
        return view('auth.login');
    });
    /*
        身份驗證
    */
    Route::auth();
Route::group(['middleware' => 'enabled'], function () {
    Route::get('/customers', ['as'=>'customers.index','uses'=>'CustomerController@index']);//顯示訂單
    Route::post('/customer', ['as'=>'ajax.search','uses'=>'CustomerController@search']);//一般搜尋
    Route::get('show/{PO}', 'CustomerController@detail');//詳細訂單資訊
    Route::post('/adsearch', ['as'=>'ajax.adsearch','uses'=>'CustomerController@adSearch']);//進階搜尋
	Route::post('ajax/customers',['as'=>'ajax.customers','uses'=>'CustomerController@ajax_customers']);
	Route::get('/changepwd', ['as'=>'changepwd','uses'=>'CustomerController@changepwd']);//變更密碼頁面
    Route::post('/changepwdsave', 'CustomerController@changepwdsave');//變更密碼
   
    /*
        簡易後台管理
    */
    Route::group(['middleware'=>'admin'],function (){
	    Route::get('/admin', ['as'=>'admin.index','uses'=>'UserController@index']);//使用者顯示
	    Route::post('/admin/create', ['as'=>'admin.create','uses'=>'UserController@create']);//使用者創建表單頁面
	    Route::patch('/admin/update', ['as'=>'admin.update','uses'=>'UserController@update']);//使用者編輯動作
	
	    Route::get('/power',['as'=>'power.index','uses'=>'PowerController@index']);//管理權限首頁
	    Route::post('/power/create', ['as'=>'power.create','uses'=>'PowerController@create']);//權限創建表單頁面
	    Route::patch('/power/update', ['as'=>'power.update','uses'=>'PowerController@update']);//權限編輯表單頁面
    });
    
	/**
	 * 分析圖表
	 */
	Route::get('analysis/{month?}',['as'=>'analysis.index','uses'=>'AnalysisController@index']);//圖表首頁
    /*
        QRcode填單系統
    */
	Route::get('/fill-form-with-qrcode/{subjectid}', 'QRcodeController@index');//qrcode表單填單頁面
	Route::POST('/fill-form-with-qrcode/action_save', 'QRcodeController@save');//insert至資料庫的動作
	Route::get('/qrcodelist', ['as'=>'qrcodelist','uses'=>'QRcodeController@qrcodelist']);//利用qrcode code提交過的紀錄
	

});



Route::get('sample-restful-apis', function()
{
    return array(
      1 => "expertphp",
      2 => "demo"
    );
});

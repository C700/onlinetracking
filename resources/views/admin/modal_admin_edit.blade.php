<div class="modal fade" id="modal_admin_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{Form::open(['method'=>'patch','route'=>'admin.update'])}}
			{{Form::text("id","",['id'=>'id','hidden'=>'hidden','v-model'=>'id'])}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title" id="exampleModalLabel">使用者資料更新</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom: 10px">
					{{Form::label("username","帳號 ： ",['class'=>'control-label'])}}
					{{Form::text("username","",['placeholder'=>'帳號','required'=>'required','id'=>'username','style'=>'width:70%','v-model'=>'username'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("email","電子郵件 ： ",['class'=>'control-label'])}}
					{{Form::email("email","",['placeholder'=>'電子郵件','required'=>'required','id'=>'email','style'=>'width:70%','v-model'=>'email'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("salesID","業務代碼 ： ",['class'=>'control-label'])}}
					{{Form::text("salesID","",['placeholder'=>'業務代碼','required'=>'required','id'=>'salesID','style'=>'width:70%','v-model'=>'salesID'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("salesName","使用者名稱 ： ",['class'=>'control-label'])}}
					{{Form::text("salesName","",['placeholder'=>'使用者名稱','required'=>'required','id'=>'salesName','style'=>'width:70%','v-model'=>'salesName'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("","啟用設定 ： ",['class'=>'control-label'])}}
					{{Form::radio('enabled', '0' , null, ['id'=>'enabled0','v-model'=>'enabled'])}}{{Form::label("enabled0","停權")}}
					{{Form::radio('enabled', '1', null , ['required'=>'required','id'=>'enabled1','v-model'=>'enabled'])}}{{Form::label("enabled1","啟用")}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("power","瀏覽權限 ： ",['class'=>'control-label'])}}
					{{Form::select("power", $powers, null,['required'=>'required','id'=>'power','v-model'=>'power'])}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				<button type="submit" class="btn btn-success">儲存</button>
			</div>
			{{Form::close()}}
		</div>
	</div>
</div>
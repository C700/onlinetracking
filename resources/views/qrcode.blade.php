<!DOCTYPE html>
<html>
<title>Infomation Input</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="UTF-8">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<body>

<div class="w3-card-4" style="max-width:600px;margin: 0 auto">
  <div class="w3-container w3-brown">
    <h2>Information Input</h2>
  </div>
  <form class="w3-container" action="{{url('/fill-form-with-qrcode/action_save')}}" method="POST" onsubmit="return formCheck();">
  	{{ csrf_field() }}
    <p>      
    <label class="w3-text-brown"><b>Subject ID</b></label>
    <input class="w3-input w3-border w3-sand" name="cc_subjectID" type="text" value="{{$subID}}" readonly></p>
    <p>      
    <label class="w3-text-brown" for="cc_name"><b>姓名</b></label>
    <input class="w3-input w3-border w3-sand" name="cc_name" id="cc_name" type="text"><b id="v_name" style="color:red"></b></p>
    <p>      
    <label class="w3-text-brown" for="cc_type"><b>檢體類別</b></label>
	<select class="w3-select w3-border" name="cc_type" id="cc_type">
	  <option value="" selected>Choose your option</option>
	@foreach($types as $row)
	  <option value="{{$row}}">{{$row}}</option>
	@endforeach
	</select><b id="v_type" style="color:red"></b></p>
    <p>      
    <label class="w3-text-brown"><b>收案醫院</b></label>
    <input class="w3-input w3-border w3-sand" name="cc_hos_name" type="text" value="{{$hospitals['hos_name']}}" required readonly></p>
    <p>      
    <label class="w3-text-brown"><b>醫院地址</b></label>
    <input class="w3-input w3-border w3-sand" name="cc_hos_addr" type="text" value="{{$hospitals['hos_add']}}" required readonly></p>
    <p>      
    <label class="w3-text-brown"><b>收案醫師</b></label>
    <select class="w3-select w3-border" name="cc_doctor" id="cc_doctor">
	  <option value="" selected>Choose your option</option>
	@foreach($hospitals['doc_name'] as $row)
	  <option value="{{$row}}">{{$row}}</option>
	@endforeach
	</select><b id="v_doctor" style="color:red"></b></p>
    <p>      
    <label class="w3-text-brown" for="cc_sales"><b>負責業務</b></label>
    <select class="w3-select w3-border" name="cc_sales" id="cc_sales">
	  <option value="">Choose your option</option>
	@foreach($sales as $row)
	  <option value="{{$row}}" {{ strtolower($row) == $user ? 'selected' : '' }} >{{$row}}</option>
	@endforeach
	</select><b id="v_sales" style="color:red"></b></p>
    <p>      
    <label class="w3-text-brown"><b>是否為急件</b></label>
    <input class="w3-radio" type="radio" name="urgent" value="N" checked>否&nbsp
    <input class="w3-radio" type="radio" name="urgent" value="R" >是</p>
    <p>      
    <label class="w3-text-brown"><b>Note</b></label>
    <input class="w3-input w3-border w3-sand" name="cc_note" type="text"></p>
    <p>
    <button class="w3-btn w3-brown">Submit</button></p>
  </form>
</div>
<script type="text/javascript">
$(document).ready(function (e) {
	
	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});	            	 					            	 			            	 
});

function formCheck()
{
	var cc_doctor = cc_name = cc_type = cc_sales = false;
	
	if($('#cc_name').val() == '' ){
		$('#v_name').text('資料尚未填寫');
		cc_name = true;
	}
	
	if($('#cc_type').val() == '' ){
		$('#v_type').text('資料尚未選取');
		cc_type = true;
	}
	
	if($('#cc_sales').val() == '' ){
		$('#v_sales').text('資料尚未選取');
		cc_sales = true;
	}
	
	if($('#cc_doctor').val() == '' ){
		$('#v_doctor').text('資料尚未選取');
		cc_doctor = true;
	}
	
	if(cc_name || cc_type || cc_sales || cc_doctor){
		return false;
	}
	
	return true;
}
</script>
</body>
</html> 
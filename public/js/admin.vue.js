new Vue({
	el:"#container_admin",
	data:{
		id:"",
		username:"",
		email:"",
		salesID:"",
		salesName:"",
		enabled:"",
		power:""
	},
	methods:{
		edit_admin(id,username,email,salesID,salesName,enabled,power) {
			this.id=id;
			this.username=username;
			this.email=email;
			this.salesID=salesID;
			this.salesName=salesName;
			this.enabled=enabled;
			this.power=power;
			$("#modal_admin_edit").modal('show');
		}
	}
});

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-body">
				  <table class="table table-striped task-table">
                     
					<thead>
                        <th>訂單狀況</th>
                        <th style="color: red;">{{$customer->REQUIREDATE}}</th>
                    </thead>
                    <tbody>
						<tr>
                            <td class="table-text"><div>客戶名稱</div></td>
                            <td>{{$customer->CUS_NAME}}({{$customer->CUS_ID}})</td>
                        </tr>
						<tr>
                            <td class="table-text"><div>地區</div></td>
                            <td>{{$customer->LOCAL}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>業務員</div></td>

                            <td>{{$customer->SALESNAME}}({{$customer->SALESID}})</td>
                        </tr>
						
						<tr>
                            <td class="table-text"><div>訂單編號</div></td>

                            <td>{{$customer->PO_TYPE.'-'.$customer->PO_ID}}</td>
                        </tr>
						
						<tr>
                            <td class="table-text"><div>訂單序號</div></td>

                            <td>{{$customer->PO_SEQ}}</td>
                        </tr>
						
						<tr>
                            <td class="table-text"><div>結案狀況</div></td>

                            <td>
							
							<!--是否已經結案(Y:已結案/N:未結案/y:強制結案/V:作廢)-->
							@if($customer->PO_CLOSE == 'Y' ) 
								<span class="label label-success">已結案</span>

							@elseif($customer->PO_CLOSE == 'y' ) 
								<span class="label label-primary">強制結案</span>
							
							@elseif($customer->PO_CLOSE == 'V' ) 
								<span class="label label-default">作廢</span>
							@elseif($customer->PO_CLOSE == 'N' ) 
								<span class="label label-warning">未結案</span>
							@endif
							
							</td>
                        </tr>
						
                        
                        
                        <tr>
                            <td class="table-text"><div>訂單日期</div></td>
                            <td>{{$customer->PO_DATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>產品</div></td>
                            <td>{{$customer->PROD_NAME}}({{$customer->PROD_ID}})</td>
                        </tr>
						
						
						
                        
                        <tr>
                            <td class="table-text"><div>採血</div></td>
                            <td>{{$customer->BLOODDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>收樣</div></td>
                            <td>{{$customer->SAMPLEDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>核酸萃取</div></td>
                            <td>{{$customer->EXTRACTIONDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>QC</div></td>
                            <td>{{$customer->QCDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>純化</div></td>
                            <td>{{$customer->PURIFYDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>晶片實驗</div></td>
                            <td>{{$customer->EXPERIMENTDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>委外日</div></td>
                            <td>{{$customer->OUTSOURCINGDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>報告製作</div></td>
                            <td>{{$customer->REPORTDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>報告簽署</div></td>
                            <td>{{$customer->SIGNDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>交付業務</div></td>
                            <td>{{$customer->TOSALESDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>寄送報告</div></td>
                            <td>{{$customer->SENDRPTDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>開立發票日</div></td>
                            <td>{{$customer->INVOICEDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>客戶要求日</div></td>
                            <td>{{$customer->REQUIREDATE}}</td>
                        </tr>
                        <tr>
                            <td class="table-text"><div>備註</div></td>
                            <td>{{$customer->NOTE}}</td>
                        </tr>
					</tbody>
                 </table>
            </div>
			
        </div>
		
		<br>
		<div class="panel panel-default">
                
                <button type="button" class="btn btn-primary pull-right" onclick="history.back()">回上一頁</button>
				
                   
        </div>
		<br>
		
    </div>
</div>
@endsection
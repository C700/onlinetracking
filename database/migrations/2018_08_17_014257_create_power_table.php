<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power', function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedTinyInteger('code')->unique()->comment('權限代碼');
	        $table->string('name')->unique()->comment('權限名稱');
	        $table->boolean('level')->default(true)->comment('true:看全部資料  ,false:自身資料');//
	        $table->boolean('admin')->default();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('power');
    }
}

<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
    	$except = class_basename(get_class($e));
//	    dd($except,$e);
	    if ($except == "TokenMismatchException") {
		    return redirect('/')->with('warning','Token無法匹配，請重新整理頁面後登入 ('.$e->getCode().')');
	    }
	    
	    if($except == "MethodNotAllowedHttpException" ){
	    	return redirect()->back()->with('warning','不允許的操作方法 ('.$e->getCode().')');
	    }
	
	    if($except ==  "NotFoundHttpException" ){
		    return redirect()->back()->with('warning','找不到路由 ('.$e->getCode().')');
	    }
	    
	    if($except == "ErrorException"){
	    	return redirect('/')->with('warning','系統出現未定義變數錯誤('.$e->getMessage().')');
	    }
	    
        return parent::render($request, $e);
    }
}

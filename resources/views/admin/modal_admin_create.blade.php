<div class="modal fade" id="modal_admin_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{Form::open(['method'=>'post','route'=>'admin.create'])}}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title" id="exampleModalLabel">創建新帳號</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom: 10px">
					{{Form::label("username","帳號 ： ",['class'=>'control-label'])}}
					{{Form::text("username","",['placeholder'=>'帳號','required'=>'required','id'=>'username_create','style'=>'width:70%'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("email","電子郵件 ： ",['class'=>'control-label'])}}
					{{Form::email("email","",['placeholder'=>'電子郵件','required'=>'required','id'=>'email_create_create','style'=>'width:70%'])}}
				</div>
				{{--<div style="margin-bottom: 10px">--}}
					{{--{{Form::label("password","密碼 ： ",['class'=>'control-label'])}}--}}
					{{--{{Form::password("password",['placeholder'=>'Ab123456','required'=>'required','id'=>'password_create','style'=>'width:70%'])}}--}}
				{{--</div>--}}
				<div style="margin-bottom: 10px">
					{{Form::label("salesID","業務代碼 ： ",['class'=>'control-label'])}}
					{{Form::text("salesID","",['placeholder'=>'業務代碼','required'=>'required','id'=>'salesID_create','style'=>'width:70%'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("salesName","使用者名稱 ： ",['class'=>'control-label'])}}
					{{Form::text("salesName","",['placeholder'=>'使用者名稱','required'=>'required','id'=>'salesName_create','style'=>'width:70%'])}}
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("enabled","啟用設定 ： ",['class'=>'control-label'])}}
					{{Form::radio('enabled', '0' , null, ['class' => 'w3-check','id'=>'enabled0_create'])}}停權
					{{Form::radio('enabled', '1', null , ['class' => 'w3-check','required'=>'required','id'=>'enabled1_create'])}}啟用
				</div>
				<div style="margin-bottom: 10px">
					{{Form::label("power","瀏覽權限 ： ",['class'=>'control-label'])}}
					{{Form::select("power", $powers, null,['required'=>'required','id'=>'power_create'])}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				<button type="submit" class="btn btn-success">新增</button>
			</div>
			{{Form::close()}}
		</div>
	</div>
</div>
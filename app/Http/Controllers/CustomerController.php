<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Tracy\Debugger;

/**
 *
 *
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
	/**
	 * 每次觸發controller就會執行
	 *
	 * CustomerController constructor.
	 */
    public function __construct()
    {
//    	Debugger::enable();
//	    Debugger::$strictMode = true;
    }
	
	/**
	 * 清除搜尋紀錄後權限判斷，撈取指定全縣的資料量丟給view
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function index()
    {
    	//清除搜尋紀錄
    	Session::forget('keyword');//模糊搜尋
	    Session::forget('adsearch');//精準比對.
	    
        $salesID = Auth::user()->Power->level?null:Auth::user()->salesID;//抓取登入者權限
        
        $customers = Customer::getCustomers($salesID);//根據權限撈資料
		foreach($customers as $key => $customer)
        {
			//字型編碼轉化
            $customers[$key]->TYPE = mb_convert_encoding($customer->TYPE,'UTF-8','BIG5');
            $customers[$key]->PROD_NAME = mb_convert_encoding($customer->PROD_NAME,'UTF-8','BIG5');
            $customers[$key]->CUS_NAME = mb_convert_encoding($customer->CUS_NAME,'UTF-8','BIG5');
            $customers[$key]->LOCAL = mb_convert_encoding($customer->LOCAL,'UTF-8','BIG5');
            $customers[$key]->SALESNAME = mb_convert_encoding($customer->SALESNAME,'UTF-8','BIG5');
            $customers[$key]->NOTE = mb_convert_encoding($customer->NOTE,'UTF-8','BIG5');
        }
        
        return view('index', [
            'customers' => $customers
        ]);
        
    }
	
	/**
	 * 透過js偵測置底，再ajax抓取下筆資料，用Session存取上次搜尋的條件及模式(none:甚麼都不搜尋，回到客戶列表、keyword:模糊搜尋、adsearch:準確查詢)
	 *
	 * @param Request $request
	 * @return array|string
	 */
    public function ajax_customers(Request $request){
	    $mode =  $request->mode;//(none:甚麼都不搜尋，回到客戶列表、keyword:模糊搜尋、adsearch:準確查詢)
	    $salesID = Auth::user()->Power->level?null:Auth::user()->salesID;//抓取登入者權限
		
	    switch ($mode){
		    case "keyword":
		        Session::forget("adsearch");//釋放精準比對的紀錄
			    $keyword = Session::get('keyword');//取得關鍵字搜尋字串
		     
			    $customers = Customer::getCustomersByKeyWord($salesID,$keyword,$request->start);//抓取特定權限與指定數量 (start:是我要從第start筆資料開始撈取
		        break;
		    case "adsearch":
			    Session::forget("keyword");//釋放一般搜尋紀錄
			    $search = Session::get("adsearch");//取得精準比對搜尋紀錄 search = [ 0:開始日期, 1:結束日期, 2:訂單狀態, 3:產品分類 4:序號 orderNo]
		    
			    $customers = Customer::getResultByAdSearch($salesID, $search[4], $search[0], $search[1], $search[2], $search[3],$request->start);
			    break;
		    case "none":
		        //釋放所有搜尋紀錄
			    Session::forget('keyword');
			    Session::forget('adsearch');
			    $customers = Customer::getCustomers($salesID,$request->start);
		        break;
	    }
	    $temp = [];
	    foreach($customers as $key => $customer)
	    {
		    //字型編碼轉化
		    $customers[$key]->TYPE = mb_convert_encoding($customer->TYPE,'UTF-8','BIG5');
		    $customers[$key]->PROD_NAME = mb_convert_encoding($customer->PROD_NAME,'UTF-8','BIG5');
		    $customers[$key]->CUS_NAME = mb_convert_encoding($customer->CUS_NAME,'UTF-8','BIG5');
		    $customers[$key]->LOCAL = mb_convert_encoding($customer->LOCAL,'UTF-8','BIG5');
		    $customers[$key]->SALESNAME = mb_convert_encoding($customer->SALESNAME,'UTF-8','BIG5');
		    $customers[$key]->NOTE = mb_convert_encoding($customer->NOTE,'UTF-8','BIG5');
		    // 塞選特定欄位資料，不要全部丟給view
		    if($mode) {
			    $temp[$key] = [];
			    $temp[$key]['serial'] = $customer -> PO;
			    $temp[$key]['close'] = $customer -> PO_CLOSE;
			    $temp[$key]['c_name'] = $customer -> CUS_NAME;
			    $temp[$key]['p_name'] = $customer -> PROD_NAME;
		    }
	    }
	
	    if (empty($temp))//無資料
		    return "empty";
	    else
		    return $temp;//丟給前端
	    
    }
	
	/**
	 * 關鍵字一般搜尋，負責處理前20筆資料的撈取，之後的要透過ajax呼叫其他function，釋放精準搜尋紀錄，並記錄關鍵字，方便ajax做下一批資料的塞選
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function search(Request $request)
    { 
        //釋放進階比對的搜尋紀錄
	    Session::forget("adsearch");

        //from POST
        $keyword =  $request->keyword?$request->keyword:null;
	    
		if(empty($keyword)){return redirect()->action('CustomerController@index')->with('warning', '請輸入關鍵字搜尋');}
	
	    $salesID = Auth::user()->Power->level?null:Auth::user()->salesID;//抓取登入者權限
    
        $customers = Customer::getCustomersByKeyWord($salesID, $keyword);
        
		if(!empty($customers)){
			foreach($customers as $key => $customer){
					$customers[$key]->TYPE = mb_convert_encoding($customer->TYPE,'UTF-8','BIG5');
					$customers[$key]->PROD_NAME = mb_convert_encoding($customer->PROD_NAME,'UTF-8','BIG5');
					$customers[$key]->CUS_NAME = mb_convert_encoding($customer->CUS_NAME,'UTF-8','BIG5');
					$customers[$key]->LOCAL = mb_convert_encoding($customer->LOCAL,'UTF-8','BIG5');
					$customers[$key]->SALESNAME = mb_convert_encoding($customer->SALESNAME,'UTF-8','BIG5');
					$customers[$key]->NOTE = mb_convert_encoding($customer->NOTE,'UTF-8','BIG5');
				}
			Session::put("keyword",$keyword);
			return view('index', [
				'customers' => $customers,
				'keyword' => $keyword
			]);
		 }else{
			return redirect()->action('CustomerController@index')->with('warning', '查無資料');
		}
    }
	
	/**
	 * 進階搜尋=>日期範圍搜尋，銷貨況狀，產品分類，先撈前20筆資料，之後的ajax去抓
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function adSearch(Request $request)
    {
	    Session::forget('keyword');//釋放一般搜尋紀錄
        //from user table
	    
        $orderNo = null;
        //from POST
		if( is_orderNo($request->orderNo)){//檢查是否符合訂單編號格式: ex:222-2017110801
            $orderNo =  str_replace("-","|", $request->orderNo);//將格式轉換
        } 
		
        $orderDate1 = $request->orderdate1 ? $request->orderdate1 : '1970-01-01 00:00:00.000'; //start date
        $orderDate2 = $request->orderdate2 ? $request->orderdate2 : date('Y-m-d').' 00:00:00.000';//end date
        $closing = $request->closing ? $request->closing : 0;//訂單狀態
        $prod_id = $request->prod_id ? $request->prod_id : 0;//產品分類
        $search =[$orderDate1,$orderDate2,$closing,$prod_id,$orderNo];//[start date,end date,訂單狀態,產品分類,訂單編號]
	       
        Session::put("adsearch",[$orderDate1,$orderDate2,$closing,$prod_id,$orderNo]);//方便ajax搜尋該紀錄內容
        
        $salesID = Auth::user()->Power->level?null:Auth::user()->salesID;//抓取登入者權限
		
		$customers = Customer::getResultByAdSearch($salesID, $search[4], $search[0], $search[1], $search[2], $search[3]);
	 
		if(!empty($customers)){
			foreach($customers as $key => $customer)
			{
				$customers[$key]->TYPE = mb_convert_encoding($customer->TYPE,'UTF-8','BIG5');
				$customers[$key]->PROD_NAME = mb_convert_encoding($customer->PROD_NAME,'UTF-8','BIG5');
				$customers[$key]->CUS_NAME = mb_convert_encoding($customer->CUS_NAME,'UTF-8','BIG5');
				$customers[$key]->LOCAL = mb_convert_encoding($customer->LOCAL,'UTF-8','BIG5');
				$customers[$key]->SALESNAME = mb_convert_encoding($customer->SALESNAME,'UTF-8','BIG5');
				$customers[$key]->NOTE = mb_convert_encoding($customer->NOTE,'UTF-8','BIG5');
			}
			return view('index', [
				'customers' => $customers,
				'keyword' => "精準比對"
			]);
		}else{
			return redirect()->action('CustomerController@index')->with('warning', '查無資料');
		}
    
    }
	/**
	 * 每筆詳細訂單紀錄
	 * PO stand for PO_TYPE|PO_ID|PO_SEQ => "訂單單別|訂單號碼|訂單序號"
	 *
	 * @param $PO
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
    public function detail($PO)
    {
        $customer = Customer::find($PO);//找到訂單
	    if (!empty($customer)) {
		    //字型編碼轉化
		    $customer -> TYPE = mb_convert_encoding($customer -> TYPE, 'UTF-8', 'BIG5');
		    $customer -> PROD_NAME = mb_convert_encoding($customer -> PROD_NAME, 'UTF-8', 'BIG5');
		    $customer -> CUS_NAME = mb_convert_encoding($customer -> CUS_NAME, 'UTF-8', 'BIG5');
		    $customer -> LOCAL = mb_convert_encoding($customer -> LOCAL, 'UTF-8', 'BIG5');
		    $customer -> SALESNAME = mb_convert_encoding($customer -> SALESNAME, 'UTF-8', 'BIG5');
		    $customer -> NOTE = mb_convert_encoding($customer -> NOTE, 'UTF-8', 'BIG5');;
		    $close = array('Y' => '已結案', 'N' => '未結案', 'y' => '強制結案', 'V' => '作廢');
		
		    $status = "";
		    if (in_array($customer -> PO_CLOSE, $close)) {//將狀態轉成對應的字串
			    $status = $close[$customer -> PO_CLOSE];
		    }
		
	        return view('show', [
			    'customer' => $customer,
			    'status' => $status
		    ]);
	    }
		return redirect()->action('CustomerController@index')->with('warning', '查無資料');

    }
	
	
	
	/* 變更密碼頁面 */
    public function changepwd()
    {
    	return redirect()->route('customers.index');
//	    return view('viewchangepwd');
    }
	
	/* 變更密碼 */
    public function changepwdsave(Request $request)
    {
		$_id = Auth::user()->id;
		$new_Password = $request->new_Password;
        $new_Password_confirm = $request->new_Password_confirm;
				
		//密碼要相同才能變更
		if($new_Password === $new_Password_confirm){
			
			User::find($_id) ->update([
				'password' => Hash::make($new_Password),
			]);
			return redirect()->action('CustomerController@index')->with('success', '密碼變更成功');
		}else{
			return redirect()->action('CustomerController@index')->with('danger', '變更密碼失敗，請通知系統管理員');
		}
    }
}
